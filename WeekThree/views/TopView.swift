import SwiftUI

struct TopView: View {
    var body: some View {
        VStack {
            NavigationView {
                VStack {
                    Image("start", bundle: .main)
                        .resizable()
                        .frame(width: 350.0, height: 350.0, alignment: .leading)
                        .padding(.top, 10)
                    Text("何を見る？")
                        .font(.title3)
                        .foregroundColor(Color.white)
                        .offset(y: -210)
                        .padding(.bottom, -210)
                    Spacer()
                    NavigationLink(
                        destination: WebView()) {
                        Text("ギャラリー")
                            .font(.title2)
                            .foregroundColor(Color.black)
                            .padding(.bottom, 25)
                    }
                    NavigationLink(
                        destination: RandomCatView()) {
                        Text("ねこ")
                            .font(.title2)
                            .foregroundColor(Color.black)
                            .padding(.bottom, 25)
                    }
                }
                .navigationBarTitle("Entrance")
            }
        }
    }
}

struct TopView_Previews: PreviewProvider {
    static var previews: some View {
        TopView()
    }
}
