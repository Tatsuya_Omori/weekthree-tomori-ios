import SwiftUI
import WebKit

struct MyWebView: UIViewRepresentable {

    let url: String
    private let observable = WebViewURLObservable()

    var observer: NSKeyValueObservation? {
        observable.instance
    }

    func makeUIView(context: Context) -> WKWebView {
        WKWebView()
    }

    func updateUIView(_ uiView: WKWebView, context: Context) {
        uiView.load(URLRequest(url: URL(string: url)!))
    }
}

private class WebViewURLObservable: ObservableObject {
    @Published var instance: NSKeyValueObservation?
}
