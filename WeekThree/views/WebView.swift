import SwiftUI

struct WebView: View {
    @State var url = "https://picsum.photos/350/350/?random"
    @State var buttonText = "始める！"
    @State var screen: CGSize!
    var body: some View {
        VStack {
            MyWebView(url: url)
                .padding(.top, 20)
                .navigationBarTitle("Gallery")
            Button(action: {
                buttonText = "もう一回!"
                url = ""
                url = "https://picsum.photos/350/350/?random"
            }) {
                VStack {
                    Text(buttonText)
                        .font(.title2)
                        .foregroundColor(Color.black)
                        .padding(.bottom, 25)
                }
            }
        }
        .onAppear(){
            screen = UIScreen.main.bounds.size
        }
    }
}

struct WebView_Previews: PreviewProvider {
    static var previews: some View {
        WebView()
    }
}
