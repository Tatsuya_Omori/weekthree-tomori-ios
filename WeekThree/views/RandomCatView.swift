import SwiftUI

struct Result: Codable {
    var file: String?
}

struct RandomCatView: View {
    @State private var results = "https://http.cat/404"
    @State var buttonText = "始める!"
    @State var screen: CGSize!
    var body: some View {
        VStack {
            MyWebView(url: results)
                .padding(.top, 20)
            Button(action: {
                loadData()
                buttonText = "もう一回!"
            }) {
                Text(buttonText)
                    .font(.title2)
                    .foregroundColor(Color.black)
                    .padding(.bottom, 25)
            }
            .navigationBarTitle("KitCat")
        }
        .onAppear(){
            screen = UIScreen.main.bounds.size
        }
        .onAppear(perform: loadData)
    }
    
    func loadData() {
        guard let url = URL(string: "https://aws.random.cat/meow") else {
            print("無効なURL")
            return
        }
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                let decoder = JSONDecoder()
                guard let decodedResponse = try? decoder.decode(Result.self, from: data) else {
                    print("Json decode エラー")
                    return
                }
                DispatchQueue.main.async {
                    results = decodedResponse.file ?? "https://http.cat/404"
                }
            } else {
                print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
            }
        }.resume()
    }
}

struct RandomCatView_Previews: PreviewProvider {
    static var previews: some View {
        RandomCatView()
    }
}
