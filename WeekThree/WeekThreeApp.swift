import SwiftUI

@main
struct WeekThreeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
